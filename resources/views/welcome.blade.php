<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        

        
    </head>
    <body>
        <div class="container">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <h2>&nbsp;</h2>
                <a class="btn btn-defaul" href="{{url('/')}}">Refresh</a>
                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kamal R Khan - Tweets</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            
                            <tbody>
                                <?php foreach($feeds as $key => $val){?>
                                <tr>
                                    <td>{{$feeds[$key]->text}}</td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
    </body>
</html>
