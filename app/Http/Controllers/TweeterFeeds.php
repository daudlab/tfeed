<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Twitter;
use App\Feed;

class TweeterFeeds extends Controller
{
    public function index(){

    	$tweets = Twitter::getUserTimeline(['screen_name' => 'kamaalrkhan', 'count' => 30, 'format' => 'object']);
    	//return $tweets_linkyfy = Twitter::linkify($tweets);

    	//return var_dump(json_decode($tweets_linkyfy));
    	
		
		$i = 0;
		
    	foreach($tweets as $key=>$val){
    		$count = \App\Feed::where('feed_id', $tweets[$key]->id)->count();
    		if($count == 0){
    			$feed = new Feed;

    			$feed->feed_id 		= $tweets[$key]->id;
	    		$feed->id_str 		= $tweets[$key]->id_str;
	    		$feed->text 		= $tweets[$key]->text;
	    		$feed->createdat 	= $tweets[$key]->created_at;
	    		$feed->save();

    		}

    		$i++;
    		
    	}

    	$feeds = \App\Feed::all()->take(10);
    	//$feeds = \App\Feed::paginate(10);
    	
	    return view('welcome')->with('feeds',$feeds);
    }
}
