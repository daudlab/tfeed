<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = [
        'feed_id', 'id_str', 'text','createdat'
    ];
    protected $hidden = [];
}
